!#/bin/bash

if test -f ~/.bashrc; then
    rm ~/.bashrc
fi
ln .bashrc ~/.bashrc

if test -f ~/.vimrc; then
    rm ~/.vimrc
fi
ln .vimrc ~/.vimrc

if test -f ~/.gitconfig; then
    rm ~/.gitconfig
fi
ln .gitconfig ~/.gitconfig

if test -f ~/.inputrc; then
    rm ~/.inputrc
fi
ln .inputrc ~/.inputrc

if ! [ -d  ~/.vim/bundle/Vundle.vim ] ; then
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi
echo please log out and back in

